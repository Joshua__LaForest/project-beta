import React from "react";

class CreateManufacturerForm extends React.Component{
    constructor(props){
        super(props);
        console.log(props)
        this.state = {
            name: '',
        };
        this.handleManufacturerNameChange=this.handleManufacturerNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleManufacturerNameChange(event){
        let value = event.target.value;
        this.setState({name: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state };
        let manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(manufacturerUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer)
            const cleared = {
                name: '',
            };
            this.setState(cleared);
        }
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input value = {this.state.name} onChange = {this.handleManufacturerNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}
export default CreateManufacturerForm