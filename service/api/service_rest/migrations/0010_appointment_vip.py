# Generated by Django 4.0.3 on 2022-09-15 03:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0009_alter_appointment_completed'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='vip',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
