from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Appointment, Technician, AppVinVO
from common.json import ModelEncoder

# Create your views here.
class AppVinVODetailEncoder(ModelEncoder):
    model = AppVinVO
    properties = ["import_href", "vin"]


class TechEncoder(ModelEncoder):
    model = Technician
    properties = [
        "tech_name",
        "tech_num"
    ]


class AppDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "owner",
        "date_time",
        "service_reason",
        "app_vin",
        "completed",
        "tech",
        "id",
        "vip",
    ]
    encoders = {
        "tech": TechEncoder()
    }

    

class AppListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "owner",
        "date_time",
        "service_reason",
        "app_vin",
        "completed",
        "tech",
        "id",
        "vip",
    ]
    encoders = {
        "tech": TechEncoder()
    }

    def get_extra_data(self, o):
        return {"tech": o.tech.tech_name}


@require_http_methods(["GET", "POST"])
def list_techs(request):
    if request.method == "GET":
        tech = Technician.objects.all()
        return JsonResponse(
            {"technicians": tech},
            encoder = TechEncoder
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechEncoder,
            safe=False,
        )
        

# @require_http_methods(["GET"])
def tech_details(request, pk):
        tech = Technician.objects.get(id=pk)
        return JsonResponse(
            {"tech": tech},
            encoder=TechEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder = AppListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            tech = Technician.objects.get(tech_num = content["tech"])
            content["tech"] = tech
       
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
            appointment,
            encoder=AppListEncoder,
            safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
            {"message": "invalid Technician"},
            status = 400,
            )

        
        

    
@require_http_methods(["GET", "PUT", "DELETE"])
def app_details(request, pk):
    if request.method == "GET":
        app = Appointment.objects.filter(app_vin = pk)
        return JsonResponse(
            app,
            encoder=AppDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        app = Appointment.objects.get(id=pk)
        return JsonResponse(
            {"app": app},
            encoder=AppDetailEncoder,
            safe=False
        )
    else:
        count, _=Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

