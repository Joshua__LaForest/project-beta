import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">   
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/appointments">Create Appointment</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/appointments/list-appointments">List Appointments</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/appointments/history">Appointment History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customer">New Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/record">New Record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/recordlist">Record List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales_person">New Seller</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create_manufacturer">New Manufacturer</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/list-manufacturers">List Manufacturers</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/list-models">List Models</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/list-automobiles">List Automobiles</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/create_vehicle_model">Add a Vehicle Model</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" to="/create_vehicle_form">Add a Vehicle</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
