import React from 'react';

function ManufacturerTable(props) {
    return (
        <table className="table table-striped"> 
            <thead>
                <th>Name</th>
            </thead>
            <tbody>
                {props.list.map(data => {
                    console.log(data)
                    return (
                        <tr key={data.id}>
                            <td>{data.name}</td>
                            
                        </tr>
                    )
                    
                })}
            </tbody>
        </table>
    );
  }
  class ListManufacturers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturerTables: [],
        };
        
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let man of data.manufacturers) {
                    const detailUrl = `http://localhost:8100/api/manufacturers/${man.id}`;
                    requests.push(fetch(detailUrl));
                    console.log(data)
                }
                const responses = await Promise.all(requests);
                const manufacturerTables = [[]];
                for (const manResponse of responses) {
                    if (manResponse.ok) {
                        const details = await manResponse.json();
                        manufacturerTables[0].push(details);
                        
                    } else {
                        console.error(manResponse)
                    }
                }
                this.setState({manufacturerTables: manufacturerTables});                
            }
        } catch (e) {
            console.error(e);
        }
    }
    render() {
        return (
            <>
              <div className="px-4 py-5 my-0 mt-0 text-center">
                <h1>Service Appointments</h1>
                <div className="col-lg-6 mx-auto">
                  <p className="lead mb-4">
                    Appointment History
                  </p>
                </div>
              </div>
              <div className="container">
                <div className="row">
                  {this.state.manufacturerTables.map((manList, id) => {
                    return (
                      <ManufacturerTable key={id} list={manList} />
                    );
                  })}
                </div>
              </div>
            </>
          );
    }
}
export default ListManufacturers;