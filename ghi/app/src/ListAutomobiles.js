import React from 'react'


function AutoTable(props) {
    return (
        <table className="table table-striped"> 
            <thead>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Color</th>
                <th>Year</th>
            </thead>
            <tbody>
                {props.list.map(data => {
                    console.log(data)
                    return (
                        <tr key={data.vin}>
                            <td>{data.model.name}</td>
                            <td>{data.model.manufacturer.name}</td>
                            <td>{data.color}</td>
                            <td>{data.year}</td>                           
                        </tr>
                    )
                    
                })}
            
            </tbody>
        </table>
    );
  }
  class ListAutomobiles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            autoTables: [],
        };        
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let auto of data.autos) {
                    const detailUrl = `http://localhost:8100/api/automobiles/${auto.vin}`;
                    requests.push(fetch(detailUrl));
                    console.log(auto.vin)
                }
                const responses = await Promise.all(requests);
                const autoTables = [[]];
                for (const autoResponse of responses) {
                    if (autoResponse.ok) {
                        const details = await autoResponse.json();
                        autoTables[0].push(details);
                        
                    } else {
                        console.error(autoResponse)
                    }
                }
                this.setState({autoTables: autoTables});   
            }
        } catch (e) {
            console.error(e);
        }
    }
    render() {
        return (
            <>
              <div className="px-4 py-5 my-0 mt-0 text-center">
                <h1>Automobiles In Our Inventory</h1>
                <div className="col-lg-6 mx-auto">
                  <p className="lead mb-4">
                    Automobiles
                  </p>
                </div>
              </div>
              <div className="container">
                <div className="row">
                  {this.state.autoTables.map((autoList, vin) => {
                    return (
                      <AutoTable key={vin} list={autoList} />
                    );
                  })}
                </div>
              </div>
            </>
          );
    }
}
export default ListAutomobiles;