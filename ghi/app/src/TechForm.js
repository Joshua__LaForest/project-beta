import React from 'react';

class TechForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tech_name: '',
            tech_num: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeNumber = this.handleChangeNumber.bind(this);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const techUrl = 'http://localhost:8080/services/techs/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(techUrl, fetchOptions);
        if (response.ok) {
            const newTech = await response.json();
            console.log(newTech)
            const clear = {
                tech_name: '',
                tech_num: '',
            };
            this.setState(clear)
        }
    }
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ tech_name: value });
    }
    handleChangeNumber(event) {
        const value = event.target.value;
        this.setState({ tech_num: value });
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create New Technician</h1>
                <form onSubmit={this.handleSubmit} id="create-tech-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeName} placeholder="Technician's Name" value = {this.state.tech_name} required type="text" name="tech_name" id="tech_name" className="form-control" />
                    <label htmlFor="tech_name">Technician's Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeNumber} placeholder="Employee ID Number" value = {this.state.tech_num} required type="text" name="tech_num" id="tech_num" className="form-control" />
                    <label htmlFor="tech_num">Employee ID</label>
                  </div>
                  <button className="btn btn-primary">Create Technician</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}
export default TechForm;