import React from 'react';

class AppForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            owner: '',
            date_time: '',
            service_reason: '',
            app_vin: '',
            technicians: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNewOwner = this.handleNewOwner.bind(this);
        this.handleNewVin = this.handleNewVin.bind(this);
        this.handleNewTime = this.handleNewTime.bind(this);
        this.handleNewReason = this.handleNewReason.bind(this);
        this.handleNewTech = this.handleNewTech.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/services/techs/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians })
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state};
        delete data.technicians;

        const appUrl = 'http://localhost:8080/services/appointments/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        
        const response = await fetch(appUrl, fetchOptions);
        if (response.ok) {
            const newApp = response.json();
            console.log(newApp)

            const clear = {
                owner: '',
                date_time: '',
                service_reason: '',
                app_vin: '',
                technicians: [],
            };
            this.setState(clear)
        }
    }

    handleNewOwner(event) {
        const value = event.target.value;
        this.setState({owner: value});
    }

    handleNewVin(event) {
        const value = event.target.value;
        this.setState({app_vin: value});
    }

    handleNewTime(event) {
        const value = event.target.value;
        this.setState({date_time: value});
    }

    handleNewReason(event) {
        const value = event.target.value;
        this.setState({service_reason: value});
    }

    handleNewTech(event) {
        const value = event.target.value;
        this.setState({tech: value});
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create New Appointment</h1>
                <form onSubmit={this.handleSubmit} id="create-tech-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNewOwner} placeholder="Owner's Name" value = {this.state.owner} required type="text" name="owner" id="owner" className="form-control" />
                    <label htmlFor="tech_name">Owner's Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNewVin} placeholder="Car VIN Number" value = {this.state.app_vin} required type="text" name="app_vin" id="app_vin" className="form-control" />
                    <label htmlFor="app_vin">Car VIN</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNewTime} placeholder="Set Date/Time" value = {this.state.date_time} required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                    <label htmlFor="date_time">Appointment Date/Time</label>
                  </div>
                  <div className="form-floating mb-3">
                    <textarea onChange={this.handleNewReason} placeholder="Reason For Service" value = {this.state.service_reason} required type="text" name="service_reason" id="service_reason" className="form-control" />
                    <label htmlFor="service_reason">Reason For Appointment</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleNewTech} value = {this.state.tech} required name="tech" id="tech.id" className="form-select">
                      <option value="">Assign Technician</option>
                      {this.state.technicians.map(tech => {
                        return (
                          <option key={tech.tech_name} value={tech.tech_num} >{tech.tech_name} | {tech.tech_num}</option>
                          )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create Appointment</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
} 
export default AppForm