# CarCar

# Josh Sales
# Megan Ehrlich: Service Microservice

## Design
### What it does?
CarCar makes it easy for car dealerships to keep track of their services and sales. 
Our app makes it easy to register technicians, make appointments, and keep track of 
appointment historys.

Our sales section Lets the client add Sales people, record transactions, and view the 
sales history for each sales person.

The Inventory side allows the client to add manufacturers, vehicle models, and specific 
vehicles, as well as view lists of everything currently in the inventory.


### Who is it for?
Carcar is for anyone who wants help organizing their car dealership.


### How to get started?
Follow these simple steps to get started using Carcar!
* [ ] Fork this repository from gitlab and save it in your profile
* [ ] Open your terminal and run the following commands to start containers to run the program
      docker volume create beta-data
      docker-compose build
      docker-compose up
      * If you are on macOS when you run docker-compose up you will see a notification 
      * about the OS variable not being named. It is safe to ignore this.
* [ ] You can create a superuser in to access the admin panel of the associated microservice api docker container
* [ ] Enjoy using CarCar! Everything should be ready for you to start organizing your business!

### What is the future like for CarCar?
In the future we will be implementing a login feature to make the site more secure!
In the meantime hopfully you're not too concerned with security.




### What we did

Team:
* [x] create a form that a list of manufacturers
* [x] create a form that creates a new manufacturer
* [x] createa a form that shows a list of vehicle models
* [x] create a form that creates a vehicle model
* [x] create a form that shows a list of vehicles in inventory
* [x] create a form that shows all vehicles in inventory

## Service microservice

Setup:
* [x] Clone repo
* [x] When using Docker, if running on macOS you will get a notification 'OS variable not set'. Safe to ignore this.
Models:
* [x] Technicians - "name", "employee_number"
* [x] Appointments - "vin", "owner", "date_time", "technician", "service_reason"
Views:
* [x] Create technician
* [x] Create appointment
  * : Check if car VIN matches dealership VINs * show VIP status
* [x] Show appointments 
* [x] Cancel appointment
* [x] Complete appointment

Frontend:
* [x] Page to let user create appointment
* [x] List appointments
* [x] Mark appointment as cancelled, or Completed
* [x] Search bar to search by vin for car's appointment history
Nav Bar:
* [x] Add link to everything from navbar





## Sales microservice

* [x] 1. git clone from repo.
* [x] 2. Backend.
* [x] create model for salesperson showing name and number
* [x] Need a view to GET, POST, PUT and DELETE for salesperson
* [x] create models for each customer showing name address and phone number
* [x] need a view to GET,  POST, PUT and DELETE for customer
* [x] create models for record that shows the vehicle by vin, the salesperson, the customer, the price
* [x] create view to GET, POST, PUT and DELETE for each vehicle by vin
* [x] create seperate view that filters vehicles by input salesperson id that shows their name
* [x] 3. create front end.
* [x] create form that creates new sales person
* [x] create form that creates new customer
* [x] create form that creates a sales record with dropdown for vehicle(using vin), dropdown for choosing salesperson, dropdown for customer(using phone number) and listing the pric
* [x] create a form that shows all sales with a search bar that filters by salespersons name.
* [x] Create nav bar handlers for each endpoint


our diagram for bounded context is in the ghi/app/srs/images folder and is titled image (7).png
