import React from "react";

class SalesPersonForm extends React.Component {
    constructor(props){
        super(props);
        console.log(props)
        this.state = {
            employee_name: '',
            employee_number: '',
        };
        this.handleEmployeeNameChange=this.handleEmployeeNameChange.bind(this);
        this.handleEmployeeNumberChange= this.handleEmployeeNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleEmployeeNameChange(event){
        let value = event.target.value;
        this.setState({employee_name: value})
    }
    handleEmployeeNumberChange(event){
        let value = event.target.value;
        this.setState({employee_number: value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state };
        let peopleUrl = 'http://localhost:8090/api/sales_people/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(peopleUrl, fetchConfig);
        if (response.ok) {
            const newPerson = await response.json();
            console.log(newPerson)
            const cleared = {
                employee_name: '',
                employee_number: '',
            };
            this.setState(cleared);
        }
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sales Person</h1>
                    <form onSubmit={this.handleSubmit} id="create-sales_person-form">
                        <div className="form-floating mb-3">
                            <input value = {this.state.employee_name} onChange = {this.handleEmployeeNameChange} placeholder="Employee Name" required type="text" name="employee_name" id="employee_name" className="form-control" />
                            <label htmlFor="employee_name">Employee Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.employee_number} onChange = {this.handleEmployeeNumberChange} placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}
export default SalesPersonForm