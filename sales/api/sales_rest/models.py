from django.db import models

# Create your models here.

class VinVO(models.Model):
    vin = models.CharField(max_length=17)
    import_href = models.CharField(max_length=45)
    
    def __str__(self):
        return self.vin

class Record(models.Model):
    automobile= models.ForeignKey(
        VinVO,
        related_name = "record",
        on_delete = models.CASCADE
    )
    employee =models.ForeignKey(
        "Sales_person",
        related_name = "salesperson",
        on_delete = models.CASCADE
    )
    customer =models.ForeignKey(
        "Customer",
        related_name = "customer_rn",
        on_delete = models.CASCADE
    )
    price = models.PositiveSmallIntegerField()
    
    def __int__(self):
        return self.automobile



class Sales_person(models.Model):
    employee_name= models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField()
    
    def __str__(self):
        return self.employee_name
    
    
class Customer(models.Model):
    customer_name = models.CharField(max_length=100)
    customer_address= models.CharField(max_length=100)
    customer_phone_number= models.CharField(max_length=12)
    
    
    def __str__(self):
        return self.customer_name