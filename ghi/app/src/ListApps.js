import React from 'react';

async function deleteApp(appointments) {
    const appUrl = `http://localhost:8080/services/appointments/${appointments.id}`;
    const fetchOptions = {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json',
        },
    }; console.log("Cancelled Appointment")
    await fetch(appUrl, fetchOptions);
    window.location.reload(true);
}
async function completeApp(appointments) {
    const appUrl = `http://localhost:8080/services/appointments/${appointments.id}/`
    const data = {"completed": true};
    const fetchOptions = {
        method: 'put',
        body: JSON.stringify(data),
    };
    const response = await fetch(appUrl, fetchOptions);
    if (response.ok) {
        const newStatus = await response.json();
        console.log(newStatus)
        window.location.reload(true);
    } else {
        console.log('error')
    }
}
class ListApps extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointmentColumns: [],
            filteredColumns: [],
            vipColumns: '',
        };
    }
    async componentDidMount() {
        const url = 'http://localhost:8080/services/appointments/';
       
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState ({appointmentColumns: data.appointments})
                const filteredColumns = []
                for (let app of this.state.appointmentColumns) {
                    if (app.completed === false) {
                        filteredColumns.push(app)
                    }
                }
                this.setState ({filteredColumns: filteredColumns})
            }
        } catch (e) {
            console.error(e);
        }   
    }
    render() {
        return (
            <>
              <div className="px-4 py-5 my-0 mt-0 text-center">
                <h1>Service Appointments</h1>
                <div className="col-lg-6 mx-auto">
                  <p className="lead mb-4">
                    Scheduled Appointments
                  </p>
                  
                </div>
              </div>
              <div className="container">
                <div className="row">
                  
                <table className="table table-striped"> 
            <thead>
                <th>Owner</th>
                <th>VIN</th>
                <th>Date</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </thead>
            <tbody>
            {this.state.filteredColumns.map(data => {
                console.log("DATA", data)
                
                return (
                    <tr key={data.id}>
                        <td>{data.owner}</td>
                        <td>{data.app_vin}</td>
                        <td>{data.date_time}</td>
                        <td>{data.tech}</td>
                        <td>{data.service_reason}</td>
                        <td><a onClick ={() => deleteApp(data)} className="btn btn-primary btn-sm px-2 gap-3">Delete</a> 
                        <button type="button" id="completed" onClick={() => completeApp(data)}> Complete </button></td>
                    </tr>
                )
            })}
            </tbody>
        </table>
                </div>
              </div>
            </>
          );
    }
}
export default ListApps;
