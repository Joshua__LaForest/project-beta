from django.contrib import admin
from .models import Sales_person, Customer, VinVO, Record
# Register your models here.

admin.site.register(Sales_person)
admin.site.register(Customer)
admin.site.register(VinVO)
admin.site.register(Record)