import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import VinVO 

# Import models from sales_rest, here.
# from sales_rest.models import Something
def get_vin():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for autos in content["autos"]:
        VinVO.objects.update_or_create(
            import_href = autos["href"],
            defaults = {"vin": autos["vin"]}
        )
        


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_vin()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(15)


if __name__ == "__main__":
    poll()
