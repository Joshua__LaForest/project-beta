import React from "react";



class CustomerForm extends React.Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            customer_name: '',
            customer_address: '',
            customer_phone_number: '',
        };
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
        this.handleCustomerPhoneNumberChange = this.handleCustomerPhoneNumberChange.bind(this);
        this.handleCustomerAddressChange = this.handleCustomerAddressChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleCustomerNameChange(event) {
        let value = event.target.value;
        this.setState({ customer_name: value })
    }
    handleCustomerAddressChange(event) {
        let value = event.target.value;
        this.setState({ customer_address: value })
    }
    handleCustomerPhoneNumberChange(event) {
        let value = event.target.value;
        this.setState({ customer_phone_number: value })
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        let customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer)
            const cleared = {
                customer_name: '',
                customer_address: '',
                customer_phone_number: '',
            };
            this.setState(cleared);
        }
    }
    render() {
        return (<div className="row" >
            <div className="offset-3 col-6" >
                <div className="shadow p-4 mt-4" >
                    <h1 > Add a Sales Person </h1>
                    <form onSubmit={this.handleSubmit}
                        id="create-sales_person-form" >
                        <div className="form-floating mb-3" >
                            <input value={this.state.customer_name} onChange={this.handleCustomerNameChange} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                            <label htmlFor="customer_name" > Customer Name </label>
                        </div>
                        <div className="form-floating mb-3" >
                            <input value={this.state.customer_address} onChange={this.handleCustomerAddressChange} placeholder="Customer Address" required type="text" name="customer_address" id="customer_address" className="form-control" />
                            <label htmlFor="customer_address" > Customer Address </label>
                        </div>
                        <div className="form-floating mb-3" >
                            <input value={this.state.customer_phone_number} onChange={this.handleCustomerPhoneNumberChange} placeholder="Customer Phone Number" required type="text" name="customer_phone_number" id="customer_phone_number" className="form-control" />
                            <label htmlFor="customer_phone_number" > Customer Phone Number </label>
                        </div>
                        <button className="btn btn-primary" > Create </button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}
export default CustomerForm