import React from "react";

class CreateVehicleForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model_id: '',
            models: [],
            vin: '',
            year: '',
            color: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state };
        delete data.models
        const vehicleUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(vehicleUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newVehicle = await response.json();
            console.log(newVehicle)
            const cleared = {
                model_id: '',
                vin: '',
                year: '',
                color: '',
            };
            this.setState(cleared);
        }
    }
    handleColorChange(event){
        const value = event.target.value;
        this.setState({color: value});
    }
    handleYearChange(event){
        const value = event.target.value;
        this.setState({year: value});
    }
    handleModelChange(event){
        let value = event.target.value;
        this.setState({model_id: value})
    }
    handleNewVin(event) {
        const value = event.target.value;
        this.setState({vin: value});
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            let models = data
            this.setState({models: data.models});
        }
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new vehicle</h1>
                    <form onSubmit={this.handleSubmit} id="create-vehicle-form">
                    <div className="form-floating mb-3">
                            <input value = {this.state.year} onChange = {(event)=> this.handleYearChange(event)} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>
                    <div className="form-floating mb-3">
                        <input onChange={(event)=>this.handleNewVin(event)} placeholder="Car VIN Number" value = {this.state.vin} required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">vin</label>
                    </div>
                    <div className="form-floating mb-3">
                            <input value = {this.state.color} onChange = {(event)=>this.handleColorChange(event)} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select value = {this.state.model_id} onChange = {(event) =>this.handleModelChange(event)} placeholder="model_id" required type="text" name="model_id" id="model_id" className="form-control">
                            <option>Choose a model</option>
                            {this.state.models.map(model_id => {
                            return (
                            <option key = {model_id.id} value = {model_id.id}>
                            {model_id.name}
                            </option>
                        );
                        })}
                        </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}
export default CreateVehicleForm