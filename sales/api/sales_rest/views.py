from django.shortcuts import render
from .models import Customer, Record, Sales_person, VinVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class Sales_personListEncoder(ModelEncoder):
    model = Sales_person
    properties = [
        "employee_name",
        "employee_number",
        "id",
    ]

    
class Sales_personDetailEncoder(ModelEncoder):
    model = Sales_person
    properties = [
        "employee_name",
        "employee_number",
        "id",
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "customer_phone_number",
        "customer_address",
        "id",
    ]

    
class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "customer_phone_number",
        "customer_address",
        "id",
    ]
 




class VinVOEncoder(ModelEncoder):
    model = VinVO
    properties = [
        "import_href",
        "vin",
    ]



class RecordListEncoder(ModelEncoder):
    model = Record
    properties = [
        "automobile",
        "employee",
        "customer",
        "price",
        "id"
        
    ]
    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "employee": o.employee.employee_name,
            "customer": o.customer.customer_name,
            }


class RecordDetailEncoder(ModelEncoder):
    model = Record
    properties = [
        "automobile",
        "employee",
        "customer",
        "price",
        "id",
    ]
    encoder = {
        "automobile": VinVOEncoder(),
        "employee": Sales_personDetailEncoder(),
        "customer": CustomerDetailEncoder()
    }
    
    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "employee": o.employee.employee_name,
            "customer": o.customer.customer_name,
        }






@require_http_methods(['GET', 'POST'])
def list_sales_person(request):
    if request.method == "GET":
        employee = Sales_person.objects.all()
        return JsonResponse(
            {"employee": employee},
            encoder = Sales_personListEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        employee = Sales_person.objects.create(**content)
        return JsonResponse(
            employee,
            encoder = Sales_personDetailEncoder,
            safe = False
        )
        
        
@require_http_methods(["GET", "PUT", "DELETE"])
def sales_person_details(request, pk):
    if request.method == "GET":
        employee = Sales_person.objects.get(id = pk)
        return JsonResponse(
            employee,
            encoder = Sales_personDetailEncoder,
            safe = False,
        )


 
@require_http_methods(['GET', 'POST'])
def list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder = CustomerListEncoder, 
            safe = False
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder = CustomerDetailEncoder,
            safe = False
        )
        
        
@require_http_methods(["GET", "PUT", "DELETE"])
def customer_details(request, pk):
    if request.method == "GET":
        person = Customer.objects.get(id = pk)
        return JsonResponse(
            person,
            encoder = CustomerDetailEncoder,
            safe = False,
        )

@require_http_methods(['GET', 'POST'])
def record_list(request):
    if request.method == "GET":
        record = Record.objects.all()
        return JsonResponse(
            {"record": record},
            encoder = RecordListEncoder,
            safe = False,
        )
    else:
        content = json.loads(request.body)
        try:
            employee = Sales_person.objects.get(id = content["employee"])
            content["employee"] = employee
        except Sales_person.DoesNotExist:
            return JsonResponse(
                {"message": "invalid sales person"},
                status = 400,
            )
        try:
            automobile = VinVO.objects.get(vin = content["automobile"])
            content["automobile"] = automobile
        except VinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid automobile"},
                status = 400,
            )
        try:
            customer = Customer.objects.get(id = content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "invalid customer"},
                status = 400,
            )
            
        record = Record.objects.create(**content)
        return JsonResponse(
            {"record": record},
            encoder = RecordDetailEncoder,
            safe = False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def records_details(request, pk):
    if request.method == "GET":
        record = Record.objects.filter(employee = pk)
        return JsonResponse(
            {"record": record},
            encoder = RecordDetailEncoder,
            safe = False,
        )
        
