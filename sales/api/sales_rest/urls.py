from django.urls import path
from .views import list_sales_person, sales_person_details, list_customers, customer_details, record_list, records_details

urlpatterns = [
    path("sales_people/", list_sales_person, name = "list_sales_people"),
    path("sales_people/<int:pk>", sales_person_details, name = "sales_person"),
    path("customers/", list_customers, name = "customers"),
    path("customers/<int:pk>", customer_details, name = "customer"),
    path("records/", record_list, name = "record_list"),
    path("records/<int:pk>", records_details, name = "record_details"),
    # path("record/<str:employee_number>/", get_record_by_name, name = "record_by_name")
]