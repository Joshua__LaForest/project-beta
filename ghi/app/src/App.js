import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechForm from './TechForm';
import AppForm from './AppForm';
import ListApps from './ListApps';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import RecordForm from './RecordForm';
import CreateManufacturerForm from './CreateManufacturerForm';
import AppHistory from './ListAppHistory';
import ListManufacturers from './ListManufacturers';
import ListModels from './ListModels';
import ListAutomobiles from './ListAutomobiles';
import RecordListForm from './RecordsListForm';
import CreateVehicleModel from './CreateVehicleModel';
import CreateVehicleForm from './CreateVehicleForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians" element={<TechForm />} />
          <Route path="appointments" >
            <Route index element={<AppForm />} />
            <Route path="list-appointments" element={<ListApps />} />
            <Route path="history" element={<AppHistory />} />
          </Route>
          <Route path="sales_person" element = {<SalesPersonForm />} />
          <Route path="customer" element = {<CustomerForm />} />
          <Route path="record" element = {<RecordForm />} />
          <Route path="recordlist" element = {<RecordListForm />} />
          <Route path = "create_manufacturer" element={<CreateManufacturerForm />} />
          <Route path="list-manufacturers" element={<ListManufacturers />} />
          <Route path="list-models" element={<ListModels />} />
          <Route path="list-automobiles" element={<ListAutomobiles />} />
          <Route path="create_vehicle_model" element={<CreateVehicleModel />} />
          <Route path="create_vehicle_form" element={<CreateVehicleForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
