import React from "react";
import {useRef, useEffect} from 'react';

class RecordForm extends React.Component {
    constructor(props){
        super(props);
            this.state = {
                automobile: '',
                autos: [],
                employee: '',
                employees: [],
                customer:'',
                customers: [],
                price: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handlePriceChange(event){
        let value = event.target.value;
        this.setState({price: value})
    }
    handleCustomerChange(event){
        let value = event.target.value;
        this.setState({customer: value})
    }
    handleSalesPersonChange(event){
        let value = event.target.value;
        this.setState({employee: value})
    }
    handleAutomobileChange(event){
        let value = event.target.value;
        this.setState({automobile: value})
    }
async componentDidMount() {
        const customerurl = 'http://localhost:8090/api/customers/';
        const customerresponse = await fetch(customerurl);

        const salesurl = 'http://localhost:8090/api/sales_people/';
        const salesresponse = await fetch(salesurl)

        const vehicleurl = 'http://localhost:8100/api/automobiles/';
        const vehicleresponse = await fetch(vehicleurl)

        if (customerresponse.ok && salesresponse.ok && vehicleresponse.ok) {
            const data = await customerresponse.json();
            let customers = data
            this.setState({customers: data.customer});
            console.log(customers)

            const salesdata = await salesresponse.json();
            let employees = salesdata
            this.setState({employees: salesdata.employee})
            console.log(employees)

            const vehicledata = await vehicleresponse.json();
            let autos = vehicledata
            this.setState({autos: vehicledata.autos})
            console.log(autos)
        }
}
async handleSubmit(event) {
    event.preventDefault();
    const recorddata = {...this.state };
    // console.log(recorddata)
    delete recorddata.autos;
    delete recorddata.customers;
    delete recorddata.employees;
    let recordUrl = 'http://localhost:8090/api/records/';
    console.log("fired")
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(recorddata),
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const recordresponse = await fetch(recordUrl, fetchConfig);
    console.log(recordresponse)
    if (recordresponse.ok) {
        // console.log(response)
        const newRecord = await recordresponse.json();
        console.log(newRecord)
        const cleared = {
            automobile: '',
            employee: '',
            customer:'',
            price: '',
        };
        this.setState(cleared);
    }
}
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Record</h1>
                <form onSubmit={this.handleSubmit} id="create-record-form">
                    <div className="mb-3">
                            <select value = {this.state.automobile} onChange = {(event)=>this.handleAutomobileChange(event)} required id="automobile" name="automobile" className="form-select" >
                        <option>Automobile</option>
                        {this.state.autos.map(automobile => {
                        return (
                            <option key = {automobile.vin} value = {automobile.vin}>
                            {automobile.vin}-{automobile.model.name}
                            </option>
                        );
                        })}
                        </select>
                        </div>
                        <div className="mb-3">
                            <select value = {this.state.employee} onChange = {(event)=>this.handleSalesPersonChange(event)} required id="employee" name="employee" className="form-select" >
                        <option>Sales Person</option>
                        {this.state.employees.map(employee => {
                        return (
                            <option key = {employee.id} value = {employee.id}>
                            {employee.employee_name}
                            </option>
                        );
                        })}
                        </select>
                        </div>
                        <div className="mb-3">
                            <select value = {this.state.customer} onChange = {(event)=>this.handleCustomerChange(event)} required id="customer" name="customer" className="form-select" >
                        <option>Choose a customer</option>
                        {this.state.customers.map(customer => {
                        return (
                            <option key = {customer.id} value = {customer.id}>
                            {customer.customer_name}
                            </option>
                        );
                        })}
                        </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.price} onChange = {(event)=> this.handlePriceChange(event)} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}
export default RecordForm




