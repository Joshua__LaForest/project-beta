import React from "react";

class CreateVehicleModel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer_id: '',
            manufacturers: [],
            name: '',
            picture_url: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleManufacturerChange(event){
        let value = event.target.value;
        this.setState({manufacturer_id: value})
    }
    handleModelNameChange(event){
        let value = event.target.value;
        this.setState({name: value})
    }
    handleChangePhoto(event){
        let value = event.target.value;
        this.setState({picture_url: value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state };
        delete data.manufacturers;
        let modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(modelUrl, fetchConfig);
        console.log('firing')
        console.log(response)
        if (response.ok) {
            console.log(response.ok)
            const newModel = await response.json();
            console.log(newModel)
            const cleared = {
                manufacturer_id: '',
                name: '',
                picture_url: '',
                
            };
            this.setState(cleared);
        }
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({ manufacturers: data.manufacturers });
                console.log(data.manufacturers)
            }
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Model</h1>
                    <form onSubmit={this.handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <select value = {this.state.manufacturer_id} onChange = {(event)=>this.handleManufacturerChange(event)} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control">
                            <option>Choose a manufacturer</option>
                            {this.state.manufacturers.map(manufacturer_id => {
                                // console.log(this.state.manufacturers)
                                console.log(manufacturer_id)
                            return (
                            <option key = {manufacturer_id.id} value = {manufacturer_id.id}>
                            {manufacturer_id.name}
                            </option>
                        );
                        })}
                        </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.name} onChange = {(event)=>this.handleModelNameChange(event)} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <label htmlFor="description">Photo URL</label>
                        <textarea value = {this.state.picture_url} onChange={(event)=>this.handleChangePhoto(event)} id="picture_url" required type="text" placeholder="picture_url" name="picture_url" className="form-control" max_length = {100} />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}
export default CreateVehicleModel