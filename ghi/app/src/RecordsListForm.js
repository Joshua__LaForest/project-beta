import React from "react";

function RecordTable(props){
    return (
        <table className="table table-striped"> 
            <thead>
                <th> Seller </th>
                <th>Customer</th>
                <th>Vin</th>
                <th>Sale price</th>
            </thead>
            <tbody>
                {props.list.map(record => {
                    // console.log(record)
                    return (
                        <tr key = {record.id}>
                            <td>{record.employee}</td>
                            <td>{record.customer}</td>
                            <td>{record.automobile}</td>
                            <td>${record.price}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}
class RecordListForm extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                recordTables: [],
                employee: '',
                employees: [],
            };
            this.handleSubmit = this.handleSubmit.bind(this);
        }
        handleSalesPersonChange(event){
            let value = event.target.value;
            this.setState({employee: value})
        }
    async componentDidMount() {
        const url = 'http://localhost:8090/api/records/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const recordTables = [[]];
                for (let record of data.record) {
                    recordTables[0].push(record);
                    // console.log(record)
                    }
                    console.log(recordTables)
                    this.setState({recordTables: recordTables});
            }}
        catch (e) {
            console.error(e);
        }
        const salesurl = 'http://localhost:8090/api/sales_people/';
        const salesresponse = await fetch(salesurl);
        if(salesresponse.ok){
            const salesdata = await salesresponse.json();
            let employees = salesdata
            this.setState({ employees: salesdata.employee})
        }
    }
    async handleSubmit(event) {
        event.preventDefault();
        const recorddata = {...this.state };
        let recordByIdurl = `http://localhost:8090/api/records/${this.state.employee}`;
        const filteredResponse = await fetch(recordByIdurl)
        if(filteredResponse.ok){
            const filteredData = await filteredResponse.json();
            // console.log(filteredData)
            const recordTables = [[]];
            for(let record of filteredData.record){
                // console.log(record)
                recordTables[0].push(record);
            }this.setState({recordTables: recordTables});
        }
    }
        render() {
            return (
                <>
                <div className="px-4 py-5 my-0 mt-0 text-center">
                    <h1>Records</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">
                        All Records
                        </p>
                    </div>
                    <form onSubmit={this.handleSubmit} id="create-record-form">
                    <div className="mb-3">
                        <select value = {this.state.employee} onChange = {(event)=>this.handleSalesPersonChange(event)} required id="employee" name="employee" className="form-select" >
                            <option>Sales Person</option>
                            {this.state.employees.map(employee => {
                            return (
                                <option key = {employee.id} value = {employee.id}>
                                {employee.employee_name}
                                </option>
                            );
                            })}
                            </select>
                            </div>
                            <button className="btn btn-primary">Filter</button>
                            </form>
                </div>
                <div className="container">
                    <div className="row">
                        {this.state.recordTables.map((recList, id) => {
                        return (
                        <RecordTable key={id} list={recList} />
                        );
                    })}
                    </div>
                </div>
                </>
            );
        }
    }
    export default RecordListForm

