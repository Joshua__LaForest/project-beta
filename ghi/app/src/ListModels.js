import React from 'react';

function VehicleTable(props) {
    return (
        <table className="table table-striped"> 
            <thead>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Photo</th>
            </thead>
            <tbody>
                {props.list.map(data => {
                    console.log(data)
                    return (
                        <tr key={data.id}>
                            <td>{data.name}</td>
                            <td>{data.manufacturer.name}</td>
                            <td><img src={data.picture_url} /></td>       
                        </tr>
                    ) 
                })}       
            </tbody>
        </table>
    );
  }
  class ListModels extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vehicleTables: [],
        };
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log(data)
                const requests = [];
                for (let vehicle of data.models) {
                    const detailUrl = `http://localhost:8100/api/models/${vehicle.id}`;
                    requests.push(fetch(detailUrl));   
                }
                const responses = await Promise.all(requests);
                const vehicleTables = [[]];
                for (const vResponse of responses) {
                    if (vResponse.ok) {
                        const details = await vResponse.json();
                        vehicleTables[0].push(details);
                    } else {
                        console.error(vResponse)
                    }
                }
                this.setState({vehicleTables: vehicleTables});
            }
        } catch (e) {
            console.error(e);
        }
    }
    render() {
        return (
            <>
              <div className="px-4 py-5 my-0 mt-0 text-center">
                <h1>Inventory</h1>
                <div className="col-lg-6 mx-auto">
                  <p className="lead mb-4">
                    Vehicle Models
                  </p>
                </div>
              </div>
              <div className="container">
                <div className="row">
                  {this.state.vehicleTables.map((vList, id) => {
                    return (
                      <VehicleTable key={id} list={vList} />
                    );
                  })}
                </div>
              </div>
            </>
          );
    }
  }
  export default ListModels;