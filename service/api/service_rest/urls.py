from django.urls import path

from .views import (list_techs, tech_details, list_appointments, app_details)

urlpatterns = [
    path("techs/", list_techs, name="list_techs"),
    path("techs/<int:pk>/", tech_details, name="tech_details"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:pk>/", app_details, name="app_details"),
]
