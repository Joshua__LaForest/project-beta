from django.db import models


class AppVinVO(models.Model):
    vin = models.CharField(max_length=17)
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    tech_name = models.CharField(max_length = 100)
    tech_num = models.CharField(max_length = 100, unique=True)

    def __str__(self):
        return self.tech_name
    
    class Meta:
        ordering = ("tech_name",)



class Appointment(models.Model):
    owner = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    service_reason = models.TextField()
    app_vin = models.CharField(max_length=17)
    completed = models.BooleanField(default=False)
    vip = models.BooleanField(default=False, null=True, blank=True)
    tech = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.owner}: {self.app_vin}"
    
    class Meta:
        ordering = ("date_time",)