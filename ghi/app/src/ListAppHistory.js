import React from 'react';

function HistoryTable(props) {
  console.log("PROPS", props)
    return (
        <table className="table table-striped"> 
            <thead>
                <th>Owner</th>
                <th>VIN</th>
                <th>Date</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </thead>
            <tbody>
                {props.list.map(data => {
                  if (data.completed === false) {
                    return (
                      <tr key={data.id}>
                          <td>{data.owner}</td>
                          <td>{data.app_vin}</td>
                          <td>{data.date_time}</td>
                          <td>{data.tech.tech_name}</td>
                          <td>{data.service_reason}</td>
                          <td>Upcoming</td>
                      </tr>
                    )
                  } else {
                    return (
                      <tr key={data.id}>
                          <td>{data.owner}</td>
                          <td>{data.app_vin}</td>
                          <td>{data.date_time}</td>
                          <td>{data.tech.tech_name}</td>
                          <td>{data.service_reason}</td>
                          <td>Completed</td>
                        </tr>
                      )
                  }
                })}
            </tbody>
        </table>
    );
}
class AppHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        historyTables: [],
        app_vin: '',
        app_vins: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeVin = this.handleChangeVin.bind(this);
  }
      async componentDidMount() {
        const url = 'http://localhost:8080/services/appointments/';
        const inResponse = await fetch(url);
        if (inResponse.ok) {
          const data = await inResponse.json();
          const completedApps = [];
          for (let app of data.appointments) {

              if (completedApps.includes(app.app_vin) === false) {
                completedApps.push(app.app_vin)
            }
          }
          let app_vins = completedApps
          this.setState({ app_vins: completedApps })
      }
      }
      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const vinUrl = `http://localhost:8080/services/appointments/${data.app_vin}`;
        const filterResponse = await fetch(vinUrl)
        if (filterResponse.ok) {
          const filterData = await filterResponse.json();
          console.log("FilterData", filterData)
          const historyTables = [[]];
          const myArray = Object.values(filterData)
          for (let a of myArray) {
                historyTables[0].push(a);
          }
          this.setState({historyTables: historyTables});
          console.log(historyTables)
        }
      }
      handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ app_vin: value })
      }
    render() {
      return (
        <>
          <div className="px-4 py-5 my-0 mt-0 text-center">
            <h1>Appointment History</h1>
            <div className="col-lg-6 mx-auto">
              <p className="lead mb-4">
                Past Appointments
              </p>
              <p>
                Please select a car VIN from the list below to see the history
              </p>
              
            </div>
          </div>
          <form onSubmit={this.handleSubmit} id="change-vin-form">
          <div className="mb-3">
            <select onChange={this.handleChangeVin} value = {this.state.app_vin} required name="app_vin" id="app_vin" className="form-select">
              <option value="">Search By Vin</option>
              {this.state.app_vins.map(app_vin => {
                return (
                  <option key={app_vin} value={app_vin} >{app_vin}</option>
                  )
              })}
            </select>
          </div>
          <button className="btn btn-primary">Search</button>
          </form>
          <div className="container">
            <div className="row">
              {this.state.historyTables.map((appointmentList, id) => {
                
                    return (
                      <HistoryTable key={id} list={appointmentList} />
                    )
              })}
            </div>
          </div>
        </>
      );
    }
}
export default AppHistory;